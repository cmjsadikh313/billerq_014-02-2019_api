<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([

    'middleware' => 'api',
    'prefix' => 'v1'//version1

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

     Route::group(['middleware' => ['auth:api']], function () {
    	//Route::resource('users', 'UserController');
		// Route::resource('customers', 'CustomerController');
		// Route::resource('products', 'ProductController');
		// Route::resource('categories', 'CategoryController');
		// Route::resource('orders', 'OrderController');
		Route::get('/cash-register/register-details', 'CashRegisterController@getRegisterDetails');
        Route::get('/cash-register/close-register', 'CashRegisterController@getCloseRegister');
        Route::post('/cash-register/close-register', 'CashRegisterController@postCloseRegister');
		Route::resource('cash-register', 'CashRegisterController');


         Route::resource('pos', 'SellPosController');

         Route::resource('categories', 'CategoryController');
         Route::get('/sells/pos/get-product-suggestion', 'SellPosController@getProductSuggestion');
         Route::get('/sells/pos/get-recent-transactions', 'SellPosController@getRecentTransactions');
         Route::get('/contacts/customers', 'ContactController@getCustomers');
         Route::get('/products/list', 'ProductController@getProducts');
         Route::get('/sells/pos/get_product_row/{variation_id}/{location_id}', 'SellPosController@getProductRow');

   });

});
